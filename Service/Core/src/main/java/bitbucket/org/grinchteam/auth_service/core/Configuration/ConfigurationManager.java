package bitbucket.org.grinchteam.auth_service.core.Configuration;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class ConfigurationManager {

    private static JSONObject configuration;

    public static JSONObject getConfiguration(){

        if(configuration == null) {
            InputStream stream = new ConfigurationManager()
                    .getClass()
                    .getClassLoader()
                    .getResourceAsStream("settings.json");
            String content = readFile(stream);
            configuration = parseContent(content);
        }
        return configuration;
    }

    private static String readFile(InputStream stream){

        StringBuilder builder = new StringBuilder();

        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream))){

            String currentLine;

            while ((currentLine = bufferedReader.readLine()) != null) {
                builder.append(currentLine);
            }

        }catch (IOException ex){
            ex.printStackTrace();
        }

        return builder.toString();
    }

    private static JSONObject parseContent(String content){

        JSONParser parser = new JSONParser();
        JSONObject object = new JSONObject();

        try {
            object = (JSONObject) parser.parse(content);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return object;
    }
}
