package bitbucket.org.grinchteam.auth_service.core.Commands;

import bitbucket.org.grinchteam.auth_service.core.Enums.ErrorCodes;
import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import bitbucket.org.grinchteam.auth_service.core.Exceptions.BadActionsException;
import bitbucket.org.grinchteam.auth_service.core.Parameters.Factory.ParamsFactory;
import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.Params;
import bitbucket.org.grinchteam.auth_service.core.Network.RequestProvider;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetCookieFunc implements Command {

    private ResourceType type;
    private String email;
    private String password;

    public GetCookieFunc(ResourceType type, String email, String password) {
        this.type = type;
        this.email = email;
        this.password = password;
    }

    @Override
    public String execute() throws BadActionsException {

        Params params = ParamsFactory.getParameters(type, email, password);
        String cookie = null;

        try{
            cookie = RequestProvider.getCookie(params);
            boolean isValidCookie = testCookie(params.getTest_path(), params.getTest_word(), cookie);

            if(!isValidCookie)
                throw new BadActionsException(ErrorCodes.Bad_Request, "Wrong pair email and password");

        }catch (UnirestException | IOException ex){
            new BadActionsException(ErrorCodes.Internal_Server_Error, "Mistakes with creation query to resource");
        }
        return cookie;
    }

    public boolean testCookie(String test_url, String test_word, String cookie) throws IOException{

        URL uri = new URL(test_url);
        HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Cookie", cookie);

        return RequestProvider.getResponse(connection).contains(test_word);
    }
}
