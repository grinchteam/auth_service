package bitbucket.org.grinchteam.auth_service.core.Parameters.Factory;

import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.CourseParams;
import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.Params;
import org.json.simple.JSONObject;

public class CourseParamsFactory extends ParamsFactory {

    private static final String SECTION_NAME = "course";

    public CourseParamsFactory(String email, String password) {
        super(email, password);
    }

    @Override
    protected Params createParams(JSONObject configuration) {

        JSONObject course = (JSONObject) configuration.get(SECTION_NAME);
        return new CourseParams(email, password, course);
    }
}
