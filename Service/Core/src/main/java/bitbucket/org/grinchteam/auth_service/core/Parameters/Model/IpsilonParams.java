package bitbucket.org.grinchteam.auth_service.core.Parameters.Model;

import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import bitbucket.org.grinchteam.auth_service.core.Network.RequestProvider;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.net.HttpURLConnection;
import java.util.HashMap;

public class IpsilonParams extends Params{

    public IpsilonParams(String email, String password, JSONObject configuration) {
        super(email, password, ResourceType.IPSILON, configuration);

        this.arguments = new HashMap<>();
        arguments.put("user[email]", email);
        arguments.put("user[password]", password);
        arguments.put("authenticity_token", getAuthenticationToken());
    }

    private String getAuthenticationToken(){
        HttpURLConnection connection = RequestProvider.createQuery(this.login_path);
        String response = RequestProvider.getResponse(connection);

        Document document = Jsoup.parse(response);
        Element form = document.getElementById("new_user");
        Element input_element = form.getElementsByAttributeValue("name","authenticity_token").first();
        return input_element.attr("value");
    }
}
