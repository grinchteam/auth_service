package bitbucket.org.grinchteam.auth_service.core.Enums;

public enum ErrorCodes {

    Bad_Request(400),
    Internal_Server_Error(500);

    private int code;
    public int getCode() {
        return code;
    }

    public String getMessage(){
        return this.name().replaceAll("_", " ");
    }

    ErrorCodes(int code) {
        this.code = code;
    }
}
