package bitbucket.org.grinchteam.auth_service.core.Network;

import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.Params;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class RequestProvider {
    public static String getCookie(Params params) throws UnirestException {
        BasicCookieStore cookieStore = new BasicCookieStore();
        Unirest.setHttpClient(org.apache.http.impl.client.HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build());

        Unirest.post(params.getLogin_path())
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("cache-control", "no-cache")
                .body(params.toString())
                .asString();

        List<Cookie> listOfCookie = cookieStore.getCookies();
        Cookie currentCookie = listOfCookie.get(0);

        StringBuilder builder = new StringBuilder();
        builder.append(currentCookie.getName())
                .append("=").append(currentCookie.getValue())
                .append("; ").append("path=")
                .append(currentCookie.getPath());

        return builder.toString();
    }

    public static HttpURLConnection createQuery(String url){

        if(url.isEmpty() && url == null)
            throw new IllegalArgumentException("Invalid URL");

        HttpURLConnection connection = null;

        try{
            URL uri = new URL(url);
            connection = (HttpURLConnection) uri.openConnection();
            connection.setRequestMethod("GET");

        }catch (IOException ex){
            ex.printStackTrace();
        }

        return connection;
    }
    public static String getResponse(HttpURLConnection connection){
        StringBuilder response = new StringBuilder();

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))){

            String line;
            while((line = reader.readLine()) != null)
                response.append(line);

        }catch (IOException ex){
            ex.printStackTrace();
        }
        return response.toString();
    }
}
