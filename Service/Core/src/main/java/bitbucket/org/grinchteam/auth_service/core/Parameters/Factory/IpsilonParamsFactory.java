package bitbucket.org.grinchteam.auth_service.core.Parameters.Factory;

import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.IpsilonParams;
import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.Params;
import org.json.simple.JSONObject;

public class IpsilonParamsFactory extends ParamsFactory{

    private static final String SECTION_NAME = "ipsilon";

    public IpsilonParamsFactory(String email, String password) {
        super(email, password);
    }

    @Override
    protected Params createParams(JSONObject configuration) {

        JSONObject ipsilon = (JSONObject) configuration.get(SECTION_NAME);
        return new IpsilonParams(email, password, ipsilon);
    }
}
