package bitbucket.org.grinchteam.auth_service.core.Parameters.Factory;

import bitbucket.org.grinchteam.auth_service.core.Configuration.ConfigurationManager;
import bitbucket.org.grinchteam.auth_service.core.Parameters.Model.Params;
import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;

import org.json.simple.JSONObject;

public abstract class ParamsFactory {

    protected String email;
    protected String password;

    protected ParamsFactory(String email, String password){
        this.email = email;
        this.password = password;
    }

    public static Params getParameters(ResourceType type, String email, String password){

        JSONObject configuration = ConfigurationManager.getConfiguration();
        ParamsFactory factory = null;

        switch (type){
            case COURSE:
                factory = new CourseParamsFactory(email, password);
                break;
            case IPSILON:
                factory = new IpsilonParamsFactory(email, password);
                break;
        }

        return factory.createParams(configuration);
    }

    protected abstract Params createParams(JSONObject configuration);

}
