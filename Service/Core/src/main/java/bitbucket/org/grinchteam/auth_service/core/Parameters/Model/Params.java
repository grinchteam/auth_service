package bitbucket.org.grinchteam.auth_service.core.Parameters.Model;

import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import org.json.simple.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public abstract class Params {

    private static final String K_N_LOGIN_PATH = "login_path";
    private static final String K_N_TEST_PATH = "test_path";
    private static final String K_N_TEST_WORD = "test_word";

    protected String login_path;
    protected String test_path;
    protected String test_word;

    public String getLogin_path() {
        return login_path;
    }
    public String getTest_path() {
        return test_path;
    }
    public String getTest_word() {
        return test_word;
    }
    public ResourceType getType() {
        return type;
    }

    protected String email;
    protected String password;
    protected ResourceType type;

    protected Map<String, String> arguments;


    public Params(String email, String password, ResourceType type, JSONObject configuration) {
        this.email = email;
        this.password = password;
        this.type = type;

        this.arguments = new HashMap<>();
        arguments.put("username", email);
        arguments.put("password", password);

        setUp(configuration);
    }

    protected void setUp(JSONObject configuration){
        login_path = configuration.get(K_N_LOGIN_PATH).toString();
        test_path = configuration.get(K_N_TEST_PATH).toString();
        test_word = configuration.get(K_N_TEST_WORD).toString();
    }

    @Override
    public String toString() {

        StringJoiner sj = new StringJoiner("&");
        for(Map.Entry<String,String> entry : arguments.entrySet()) {
            try {
                sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
                        + URLEncoder.encode(entry.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return sj.toString();
    }
}
