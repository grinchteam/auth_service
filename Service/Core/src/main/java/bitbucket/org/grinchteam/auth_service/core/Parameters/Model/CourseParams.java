package bitbucket.org.grinchteam.auth_service.core.Parameters.Model;

import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import org.json.simple.JSONObject;

public class CourseParams extends Params {

    public CourseParams(String email, String password, JSONObject configuration) {
        super(email, password, ResourceType.COURSE, configuration);
    }
}
