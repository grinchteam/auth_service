package bitbucket.org.grinchteam.auth_service.core.Commands;

import bitbucket.org.grinchteam.auth_service.core.Exceptions.BadActionsException;

public interface Command {
    String execute() throws BadActionsException;
}
