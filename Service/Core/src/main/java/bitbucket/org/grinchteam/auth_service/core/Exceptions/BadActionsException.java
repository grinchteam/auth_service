package bitbucket.org.grinchteam.auth_service.core.Exceptions;

import bitbucket.org.grinchteam.auth_service.core.Enums.ErrorCodes;

public class BadActionsException extends Exception {

    private ErrorCodes code;
    public ErrorCodes getCode() {
        return code;
    }

    public BadActionsException(ErrorCodes code, String innerException) {
        super(code.getMessage() + ". " + innerException);
        this.code = code;
    }
}
