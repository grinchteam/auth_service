package bitbucket.org.grinchteam.auth_service.web.Servlets;

import bitbucket.org.grinchteam.auth_service.core.Commands.Command;
import bitbucket.org.grinchteam.auth_service.core.Commands.GetCookieFunc;
import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import bitbucket.org.grinchteam.auth_service.core.Exceptions.BadActionsException;
import bitbucket.org.grinchteam.auth_service.web.Exception.InvalidParametersException;
import bitbucket.org.grinchteam.auth_service.web.Parameters.CommandParameters;
import bitbucket.org.grinchteam.auth_service.web.Responses.BadResponse;
import bitbucket.org.grinchteam.auth_service.web.Responses.CookieResponse;
import bitbucket.org.grinchteam.auth_service.web.Responses.Response;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class GetCookie extends ApiController{

    @Override
    public CommandParameters handleParams(HttpServletRequest req) throws InvalidParametersException {

        String email = req.getParameter("email"),
                password = req.getParameter("password"),
                typeName = req.getParameter("type");

        if(typeName == null)
            throw new InvalidParametersException("type");
        else if(email == null)
            throw new InvalidParametersException("email");
        else if(password == null)
            throw new InvalidParametersException("password");

        ResourceType type = ResourceType.valueOf(typeName);
        CommandParameters parameters = new CommandParameters();
        parameters.setParameters(
                parameters. new Pair("type", type),
                parameters. new Pair("email", email),
                parameters. new Pair("password", password)
        );

        return parameters;
    }

    @Override
    public Response generateResponse(CommandParameters parameters) throws BadActionsException {

        Map<String, Object> pairs = parameters.getParameters();
        Response response;

        try{
            ResourceType currentType = (ResourceType) pairs.get("type");

            Command getCookie = new GetCookieFunc(
                    currentType,
                    pairs.get("email").toString(),
                    pairs.get("password").toString()
            );

            String cookie = getCookie.execute();
            response = new CookieResponse(currentType, cookie);

        }catch (BadActionsException ex){
            response = new BadResponse(ex.getCode().getCode(), ex.getMessage());
        }

        return response;
    }
}
