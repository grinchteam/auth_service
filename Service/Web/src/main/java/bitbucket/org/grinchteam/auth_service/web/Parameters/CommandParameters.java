package bitbucket.org.grinchteam.auth_service.web.Parameters;

import java.util.HashMap;
import java.util.Map;

public class CommandParameters {

    private Map<String, Object> parameters;

    public CommandParameters() {
        parameters = new HashMap<>();
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Pair ... pairs) {
        for (int i = 0, size = pairs.length; i < size; i++) {
            parameters.put(pairs[i].key, pairs[i].value);
        }
    }

    public class Pair<T>{
        private final String key;
        private final T value;

        public Pair(String key, T value) {
            this.key = key;
            this.value = value;
        }
    }
}
