package bitbucket.org.grinchteam.auth_service.web.Exception;

import bitbucket.org.grinchteam.auth_service.core.Enums.ErrorCodes;
import bitbucket.org.grinchteam.auth_service.core.Exceptions.BadActionsException;

public class InvalidParametersException extends BadActionsException {

    public InvalidParametersException(String parameterName) {
        super(ErrorCodes.Bad_Request, String.format("Invalid parameter with name %s", parameterName));
    }
}
