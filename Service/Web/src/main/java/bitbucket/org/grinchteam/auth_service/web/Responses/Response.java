package bitbucket.org.grinchteam.auth_service.web.Responses;

public interface Response {
    public String getJsonResponse();
}
