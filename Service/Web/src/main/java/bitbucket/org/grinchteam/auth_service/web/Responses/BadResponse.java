package bitbucket.org.grinchteam.auth_service.web.Responses;

import org.json.simple.JSONObject;

public class BadResponse implements Response {

    private int errorCode;
    private String message;

    public BadResponse(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    @Override
    public String getJsonResponse() {

        JSONObject output = new JSONObject();
        JSONObject result = new JSONObject();

        result.put("status", errorCode);
        result.put("message", message);
        output.put("result", result);

        return output.toJSONString();
    }
}
