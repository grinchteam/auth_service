package bitbucket.org.grinchteam.auth_service.web.Servlets;

import bitbucket.org.grinchteam.auth_service.core.Exceptions.BadActionsException;
import bitbucket.org.grinchteam.auth_service.web.Exception.InvalidParametersException;
import bitbucket.org.grinchteam.auth_service.web.Parameters.CommandParameters;
import bitbucket.org.grinchteam.auth_service.web.Responses.BadResponse;
import bitbucket.org.grinchteam.auth_service.web.Responses.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class ApiController extends HttpServlet {

    @Override
    protected final void doGet(HttpServletRequest req, HttpServletResponse resp){
        processing(req, resp);
    }
    @Override
    protected final void doPost(HttpServletRequest req, HttpServletResponse resp){
        processing(req, resp);
    }

    private void processing(HttpServletRequest req, HttpServletResponse resp){

        resp.setContentType("application/json; charset=UTF-8");
        Response response;

        try{
            CommandParameters parameters = handleParams(req);
            response = generateResponse(parameters);

        }catch (BadActionsException ex){
            response = new BadResponse(ex.getCode().getCode(), ex.getMessage());
        }

        try(PrintWriter out = resp.getWriter()){
            out.println(response.getJsonResponse());
        }catch (IOException ex){
            System.err.println(new BadResponse(500, "IOException"));
        }
    }
    public abstract CommandParameters handleParams(HttpServletRequest req) throws InvalidParametersException;
    public abstract Response generateResponse(CommandParameters parameters) throws BadActionsException;

}
