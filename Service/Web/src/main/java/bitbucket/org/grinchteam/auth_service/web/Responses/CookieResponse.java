package bitbucket.org.grinchteam.auth_service.web.Responses;

import bitbucket.org.grinchteam.auth_service.core.Enums.ResourceType;
import org.json.simple.JSONObject;

import java.util.Date;

public class CookieResponse implements Response {

    private ResourceType type;
    private String cookie;

    public CookieResponse(ResourceType type, String cookie) {
        this.type = type;
        this.cookie = cookie;
    }

    @Override
    public String getJsonResponse() {
        JSONObject output = new JSONObject();

        JSONObject result = new JSONObject();
        result.put("status", 200);

        JSONObject cookieJSON = new JSONObject();
        cookieJSON.put("type", type.toString());
        cookieJSON.put("cookie", cookie);
        cookieJSON.put("expired_date", new Date().getTime());

        result.put("cookie", cookieJSON);
        output.put("result", result);

        return output.toJSONString();
    }
}
